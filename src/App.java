public class App {
    public static void main(String[] args) throws Exception {
        
        //DECLARANDO ARREGLOS
        int numeros[] = {10,20,30,40,50,60,70,80};
        double[] decimales = new double[5];

        System.out.println(numeros[0]);
        System.out.println(numeros[1]);
        System.out.println(numeros[2]);

        //Actualizar/sobreescribir datos del arreglo
        numeros[0] = 15;
        System.out.println(numeros[0]);

        decimales[0] = 100;
        decimales[1] = 200;
        decimales[2] = 300;
        decimales[3] = 400;
        decimales[4] = 500;
        System.out.println("****************************************");
        incremental_posincremental();
        System.out.println("****************************************");
        recorrer_areglos();
        System.out.println("****************************************");
        int[] respuesta = reporte(numeros);
        for(int i = 0; i < respuesta.length; i++){
            System.out.println(respuesta[i]);
        }
    }

    public static void incremental_posincremental(){
        int i = 0;
        i += 1;
        System.out.println(i);
        //Incremental
        System.out.println( ++i );
        //posIncremental
        System.out.println( i++ );
        System.out.println(i);
    }

    public static void recorrer_areglos(){
        int[] numeros = new int[100];
        int tamanio = numeros.length;
        //Ciclo for
        for(int i = 0; i < tamanio; i++){
            numeros[i] = i*10;
            System.out.println( numeros[i] );
        } 
    }

    //Método que recibe como parámetro un arreglo de tipo entero
    public static int[] reporte(int[] ventas){
        int[] respuesta = new int[3];
        int suma = 0;
        int ventas_bajas = ventas[0];
        int ventas_altas = ventas[0];
        //Iterar ventas
        for(int i = 0; i < ventas.length; i++){
            suma += ventas[i];
            //Obtener las ventas mas bajas
            if(ventas[i] < ventas_bajas){
                ventas_bajas = ventas[i];
            }
            //Obtener las ventas mas altas
            if(ventas[i] > ventas_altas){
                ventas_altas = ventas[i];
            }

        }
        //Añadir sumatoria al arreglo
        respuesta[0] = suma;
        respuesta[1] = ventas_bajas;
        respuesta[2] = ventas_altas;
        return respuesta;
    }
    /* public static int reporte(int[] ventas){
        int suma = 0;
        //Iterar ventas
        for(int i = 0; i < ventas.length; i++){
            suma += ventas[i];
        }
        //System.out.println("Ventas totales: "+suma);
        return suma;
    } */

}
